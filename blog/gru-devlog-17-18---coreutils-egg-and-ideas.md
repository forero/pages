GRU Devlog 17-18 - coreutils, EGG and ideas

Last week I didn't make devlog because was busy and haven't done anything useful except 
mkdir for coreutils.

This week I started from try to make something in framebuffer but currently nothing, because 
I can't even understand why my code can't see size of my display. I think E will require making
something like Elib for easier communicating with E (but it will be in very far future...)

Then I made echo and very simple ls for coreutils. I am going to add more flags to ls next week.
This week I also have made simple irc bot in python, so I will try to make simple clone of suckless ii
on C (i think name gic is now really good so if you have ideas please suggest them to me). 
Currently I don't reallly understand how to work with sockets but I will try to do something.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, coreutils, egg
