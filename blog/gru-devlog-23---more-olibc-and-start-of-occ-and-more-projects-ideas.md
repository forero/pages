GRU Devlog 23 - more olibc and start of occ (and more projects ideas!)

I think this week was productive. I have done a lot of things to olibc. First I finally
implemented working FILE struct. That required to implement malloc and free, so we also
have it now! Then stdio.h was implemented. Still need to make a lot of functions, but
we already have most useful ones (for examples every C11 printf functions implemented and
fully working). Also finally stdin/stdout/stderr file descriptors working properly.

We have full implemented <ctype.h> header now! It wasn't really hard.

This year I started from tryes to make occ - GRU C compiler (maybe it will be GRU Compiler
Collection in future). A lot of things will need to be implemented and a lot of new to know!
I hope it will be fun :) 

Happy New 2022 Year! I hope we will make a lot of new tools this year. And more people will
join us :)

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, olibc, occ
