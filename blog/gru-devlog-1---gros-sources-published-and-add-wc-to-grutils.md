GRU DevLog 1 - gros sources published and add wc to grutils

I didn't make a lot work this week, so i think this devlogs should be renamed from "week number" to just "number"
Ok, so what i did this week:
- Add wc to grutils
- Published gros 
- Add basic shell and some commands for gros

I know that is not a lot, but i was reading some osdev articles and books.
Currently I would like to implement basic filesystem but i don't know what to do.

Some goals for gros (I will add that to gros readme):
- Filesystem (FAT or Ext2)
- ELF or other executable formats
- LibC (for compiling C programs)
- Maybe basic networking but after all of that

All code in osdev wiki is for C so I need to implement it myself (or port existing rust code)

If you can help me somehow please contact me via email (g1n@ttm.sh) or in other ways (~chat irc, xmpp)

tags: gru, grutils, gros
