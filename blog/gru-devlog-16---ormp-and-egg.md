GRU Devlog 16 - ormp and EGG

This week i was working on one more new project - ormp. It will be terminal multiplexor.
Currently i am doing it in ncurses. For current time it is not working - can just handle input but not display it.

Also i have an idea for new projects - EGG (Extended/Exciting GRU GUI). I am planning some proj in this category - eggwm, eggterm, eggmenu and E.
E would be a new display server. We will need to have it for Orion in future. Currently i am planning to do it or on fbdev or on DRM/KMS.

Also now yemu can be compiled with different compilers, -pedantic flag was added to makefile and all errors fixed.

And now our channels are bridged via matterbridge: irc on ~chat, irc on libera and xmpp muc.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, ormp, egg, yemu, xmpp, irc
