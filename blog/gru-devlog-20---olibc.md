GRU Devlog 20 - olibc

This week I was making olibc. I was implementing <string.h>. Also I was fixing liblinux to 
be able to include it without need to use full path in `#include`. I had several other issues 
with it and also fixed them.

My current goal is to implement <string.h> and after that I am planning to publish it.
Currently most of funcs are implemented. Making strtok now but all other functions need
locale.h and I don't know yet how it will be implemented. But it is already cool that we
have most of functions that should be in <string.h>!

Also we now have [gru webpage on codeberg](https://gru.codeberg.page)! Maybe next week I 
will move all devlogs to gru webpage so it will be easier to move. 

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, olibc, liblinux
