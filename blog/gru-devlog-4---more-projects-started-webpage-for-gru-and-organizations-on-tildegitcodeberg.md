GRU Devlog 4 - more projects started, webpage for GRU and organizations on tildegit/codeberg

In first days of this week I thought it will be not very productive.
But I was wrong. 

We now have webpage and organizations on tildegit/codeberg:

[https://g1n.ttm.sh/gru/](https://g1n.ttm.sh/gru/)

Also I am working on some new projects:

- orcc - GRU/Orion Compilers Collection (but currently I am working only on lexer so it is not published yet)

- gasm - GRU assembler (maybe will be part of GRU binutils. Also not published yet)

- hexutils - I think hexdump, xxd and some other utils will be in this project

Also I tryed to advertize GRU on ~chat, some users liked us, 
so maybe soon someone will help me with all this.

And of course if you can help me please contact me in someway! :)

tags: gru, orcc, gasm, hexutils, webpage
