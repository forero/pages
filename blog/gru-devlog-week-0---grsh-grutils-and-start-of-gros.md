GRU DevLog Week 0 - grsh, grutils and start of gros

So I have started new project this week - grsh! It is shell written on Rust. Also, for now, other rewrites of coreutils are there.

What grsh can do for this moment: echo some variables (pwd, user, home, status of previous command using $?) and text; true, false, : - set status; pwd shows current dir, also comments and cd working, basic piping (thanks to [that article](https://www.joshmcguigan.com/blog/build-your-own-shell-rust/) and of course executing not builtin command also working.

Some grutils that I made: ls, touch, mkdir/rmdir, rm, grep, head/tail, yes, cat.

TODOs exist in grsh repo - so you can read there some of my plans.

Yesterday i have started GROS - OS on Rust! But I haven't published code yet because it not have that minimum of funcionality that i want. Thanks for that site for teaching how to make basics - [https://os.phil-opp.com](https://os.phil-opp.com)

So it was first of GRU Devlog! Thanks for reading! If you want to contribute to some of that project write me an email - [g1n@ttm.sh](mailto:g1n@ttm.sh) , or contact me in other ways

tags: gru, grsh, grutils, gros
